package javaPractice;
import java.util.Scanner;
public class StringPrograms {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		StringPrograms sp = new StringPrograms();
		//sp.pro1();
		sp.pro2();
	}
	//given Sting how many vowels present in there or Not. 
	private void pro2() {
		System.out.println("Enter a string");
		String s1 = sc.nextLine();
		String s = s1.toLowerCase();
		String vowels = "aeiou";
		int count = 0;
		for(int i=0;i<s.length();i++) {
			for(int j=0;j<vowels.length();j++) {
				if(s.charAt(i)==vowels.charAt(j)) {
					count++;
				}
			}
		}
		if(count!=0)
			System.out.println("The vowels count is "+count);
		else
			System.out.println("No vowels");
		
	}
	//Given two strings s and t, return true if t is an anagram of s, and false otherwise.
	private void pro1()
	{
		System.out.println("Enter 1st Word ");
		String s=sc.nextLine();
		System.out.println("Enter 2nd Word ");
		String t=sc.nextLine();
		if(s.length()!=t.length())
			System.out.println("Please enter same Input ");
		else 
		{
			String s1 = s.toUpperCase();
			String t1 = t.toUpperCase();
			String temp = t1;//compare 		
			for(int i = 0;i < s1.length();i++) {
				for(int j = 0;j < t1.length();j++) {
					if(s1.charAt(i) == t1.charAt(j)) {
						//temp=temp.replaceFirst(""+s1.charAt(i), "");//old way
						temp = temp.replaceFirst(Character.toString(s1.charAt(i)), "");
					}
				}
			}
			if(temp.length()==0)
				System.out.println("Given word is Anagram");
			else
				System.out.println("Given word is  Not a Anagram");
		}
		
	}
	
}
