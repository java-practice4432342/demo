package javaPractice;

import java.util.Scanner;

public class PatternPrograms {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		PatternPrograms pp =new PatternPrograms();
		//pp.pro1();
		pp.pro2();
	}
	
	private void pro2() {
		String s = "CODEBOARD";
		for(int i=0;i<s.length();i++) {
			for(int j=0;j<=i;j++) {
				System.out.print(s.charAt(j));
			}
			System.out.println();
		}
		
	}

	//write a program to print following output for the given input.you can assume the string if of odd length(ex->12345)
	//1     5
	// 2   4
	//   3
	// 2   4
	//1     5
	private void pro1() 
	{
		System.out.println("Enter a string ");
		String s = sc.nextLine();
		if(s.length()%2==0) {
			System.out.println("please enter odd string number ");
		}
		else {
			System.out.println("output pattern");
			for(int row=0;row<s.length();row++) {
				for(int col=0;col<s.length();col++) {
					if(row==col || row+col==s.length()-1)
						System.out.print(s.charAt(col)+" ");
					else
						System.out.print("  ");
				}
				System.out.println();
			}
		}
	}

}
