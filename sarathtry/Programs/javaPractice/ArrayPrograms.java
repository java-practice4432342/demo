package javaPractice;
import java.util.Scanner;
public class ArrayPrograms {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		ArrayPrograms ap = new ArrayPrograms();
		//ap.pro1();
		ap.pro2();
	}
	//given value for X,Y an array A of size N,print the values that are in between X,Y.(where X<Y)
	//N=5,X=4,Y=10     //N=6 X=10 Y=14
	//A=4,3,7,11,50,1  //4,3,7,11,50,1
	//output:7         //output:11
	private void pro2() {
		System.out.println("Enter a arr size");
		int N = sc.nextInt();
		System.out.println("enter number");
		int[] A = new int[N];
		for(int i=0;i<N;i++) {
			A[i] = sc.nextInt();
		}
		System.out.println("Enter X value");
		int X = sc.nextInt();
		System.out.println("Enter Y value");
		int Y = sc.nextInt();
		for(int i=0;i<A.length;i++) {
			for(int j=X+1;j<Y;j++) {
				if(A[i]==j) {
					System.out.println("Output "+A[i]);
				}
			}
		}
		
	}
	//form a given of integers print the next bigger number for each number in the array. 
	private void pro1() 
	{
		System.out.println("Enter Array size ");
		int size = sc.nextInt();
		System.out.println("Enter "+size+" digit array elements ");
		int arr[] = new int[size];
		for(int i=0;i<size;i++) {
			arr[i]=sc.nextInt();
		}
		//{8,4,1,9,6,2}=>input
		//{0,1,2,3,4,5}=>index
		//{8->9,4->6,1->2,9-> ,6->8,2->4}//output
		for(int i=0;i<arr.length;i++) {
			int big = 0,count=0;//1
			for(int j=0;j<arr.length;j++) {
				if(arr[i] < arr[j]) { //6<9
					if(count==0) {
						big = arr[j];//8
						count++;
					}
					else {
						if(big>arr[j])
							big=arr[j];
					}
					
				}
			}
			if(count==0)
				System.out.println(arr[i]+"-> "+" ");
			else
				System.out.println(arr[i]+"-> "+big+" ");
		}
	}

}
