package patternProgram;

public class LeftTrainglePattern 
{
	public static void main(String[] args)
	{
		LeftTrainglePattern ltp = new LeftTrainglePattern();
		//ltp.name();
		ltp.lefttraiangle();

	}
	private void lefttraiangle()
	{
		
	for(int i=0; i<=6;i++)
	{
		for (int j = 2*(6-i); j >= 0; j--) 
		{
			System.out.print(" ");
		}
		for(int k=0; k <= i; k++ ) 
		{
			System.out.print("* ");
		}
		System.out.println();
	}
	}
	public void name() 
	{
		int i,j,row = 6;
		//outer loop for rows
		for (i = row; i>=0; i--)
		{
			//inner for loop for colums
			for (j = i; j >= 0 ; j--) 
			{
				//print stars
				System.out.print("* ");
			}
			//throws the cursor in a new line after printing each line
			System.out.println();
		}
	}
}
