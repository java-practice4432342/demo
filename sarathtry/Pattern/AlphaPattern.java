public class AlphaPattern{


	public static void main(String[] arg){
		//System.out.println("hi sarath");
		AlphaPattern ap = new AlphaPattern();
		ap.alpha1();
		ap.alpha2();
		ap.alpha3();
		ap.alpha4();
		ap.alpha5();
		ap.alpha6();
		ap.alpha7();
		ap.alpha8();
		ap.alpha9();
		ap.alpha10();
	}
	public void alpha10(){
		for(int i=0;i<5;i++){
			for(int j=5;j>0;j--){
				System.out.print(j+" ");
			}
			System.out.println(); 
		}
		System.out.println("-------------------------------------------------------");
	}
	public void alpha9(){
		for(int i=5;i>=1;i--){
			for(int j=1;j<=5;j++){
				System.out.print(i+" ");
			}
			System.out.println();
		}
		System.out.println("-------------------------------------------------------");
	}
	public void alpha8(){
		for(int i=1;i<=5;i++){
			for(int j=1;j<=5;j++){
				System.out.print(j+" ");
			}
			System.out.println();
		}
		System.out.println("-------------------------------------------------------");
	}
	public void alpha7(){
		for(int i=1;i<=5;i++){
			for(int j=1;j<=5;j++){
				if(i>=j)
					System.out.print(i+" ");
				else
					System.out.print("1 ");
			}
			System.out.println();
		}
		System.out.println("-------------------------------------------------------");
	}
	public void alpha6(){
		char c='A';
    	int n=5;
    	for(int i=0;i<n;i++,c++) {
      		char cop=c;
      		for(int j=0;j<n;j++) {
        		System.out.print(cop+" ");
        		cop+=5;
      		}
      		System.out.println();
		}
		System.out.println("-------------------------------------------------------");
	}
	public void alpha5(){
		for(int i=1;i<=5;i++){
			for(int j=1;j<=5;j++){
				if(i==j)
					System.out.print(i+" ");
				else
					System.out.print(1+" ");
			}
			System.out.println();
		}
		System.out.println("-------------------------------------------------------");
	}
	public void alpha4(){
		for(int i=1;i<=5;i++){
			for(int j=0;j<5;j++){
				System.out.print(i+" ");
			}
			System.out.println();
		}
		System.out.println("-------------------------------------------------------");
	}
	public void alpha3(){
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				System.out .print("* ");
			}
			System.out.println();
		}
		System.out.println("-------------------------------------------------------");
	}
	public void alpha2(){
		char a='A';
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				System.out.print(a+" ");
				a+=5;
			}
			System.out.println();
			a-=24;
		}
		System.out.println("-------------------------------------------------------");
	}
	public void alpha1(){
		char a='A';
		for(int i=1;i<=5;i++){
			for(int j=1;j<=5;j++){
				System.out.print(a+" ");
				a++;
			}
			System.out.println();
		}
		System.out.println("-------------------------------------------------------");
	}
}