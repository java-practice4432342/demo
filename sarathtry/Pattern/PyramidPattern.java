package patternProgram;

public class PyramidPattern 
{
	public static void main(String[] args) 
	{
		for(int i=0;i<=6;i++)
		{
			for(int j=6;j >= 1;j--) 
			{
				System.out.print(" ");
			}
			for(int k=0; k<=6; k++)
			{
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}
