package patternProgram;

public class RightTrianglePattern 
{
	public static void main(String[] args)
	{
		
		//outer loop for rows
		for (int i = 0; i<6; i++)
		{
			//inner for loop for colums
			for (int j = 0; j <= i; j++) 
			{
				//print stars
				System.out.print("* ");
			}
			//throws the cursor in a new line after printing each line
			System.out.println();
		}

	}

}
