public class Mobile
{
	// Declare Filed Variable
	static String mobileName = "Nokia";
	static String modelNo = "1100";
	
	//Declare instance Variable
	String friendName = "Friend";
    	long friendNumber = 987654321;
	String teacherName = "Teacher";
	long teacherNumber = 123456789;
	
	public static void main(String[] args)
	{
        //this is local variable.
        String myName="sharat";
        long myConduct= 897654321;
       
		Mobile conduct = new Mobile(); // New object creation or declareation
		
		//Printing Field variable
		System.out.println("MobileName = "+" "+ Mobile.mobileName);
		System.out.println("ModelNumber = "+" "+ Mobile.modelNo);
		
		//Printing object variable
		System.out.println("FriendName = "+" "+ conduct.friendName);
		System.out.println("FriendNumber = "+" "+ conduct.friendNumber);

		System.out.println("TeacherName = "+" "+ conduct.teacherName);
		System.out.println("TeacherNumber = "+" "+ conduct.teacherNumber);

        //print local variable 
		System.out.println("myName ="+" "+ myName);
		System.out.println("myConduct ="+" "+ myConduct);
		
	}
}

