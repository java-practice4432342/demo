package com.library.libray.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.library.libray.entity.LibraryEntity;
import com.library.libray.repositary.LibraryRepositary;

@Controller
public class LibraryController 
{
	@Autowired
	LibraryRepositary lib;
	
	@RequestMapping("index")
	public ModelAndView view() 
	{
		ModelAndView mav = new ModelAndView();
		mav.addObject("data",lib.findAll());
		return mav;
		
	}
	@GetMapping("/add")
	public ModelAndView adddata(LibraryEntity ad)
	{
		ModelAndView mav=new ModelAndView("add");
		mav.addObject("adboo",ad);
		return mav;
	}
	@PostMapping("/add") 
	public String adda(LibraryEntity ad) {
		lib.save(ad);
		return "redirect:/index";
	}
	
	
	@GetMapping("/update/{no}")
	public ModelAndView update(@PathVariable int no) 
	{
		ModelAndView mav = new ModelAndView("update");
		mav.addObject("udata",lib.findById(no).get());
		return mav;
	}
	@PostMapping("/update/{id}")
	public String updateData(@PathVariable int id,LibraryEntity ae) {
		ae.setBook_id(id);
		lib.save(ae);
		return "redirect:/index";
	}
	@GetMapping("/delete/{id}")
	public String deleteData(@PathVariable int id) 
	{
		lib.deleteById(id);
		return "redirect:/index";
	}
}
