package com.library.libray.repositary;

import org.springframework.data.jpa.repository.JpaRepository;

import com.library.libray.entity.LibraryEntity;

public interface LibraryRepositary extends JpaRepository<LibraryEntity, Integer> {

}
