package condition.number;
public class If_condition 
{
	public static void main(String[] args)
	{
		If_condition ifcon = new If_condition();
		ifcon.checkGreat(); ifcon.check2equal(); ifcon.checkEql6Grt();
	}
	public void checkGreat()
	{
		int no1 = -11; int no2 = -10;
		if(no1>no2) 
			System.out.println("Enter no1="+ no1 +" is Grater");
		else 
			System.out.println("Enter no2="+ no2 +" is Grater");
	}
	public void check2equal() 
	{
		int n1 = 5; int n2 = 15;
		if(n1==n2) 
			System.out.println("Given n1=n2 "+n1+"="+n2);
		else 
			System.out.println("Given n1!=n2 "+n1+"!="+n2);
	}
	private void checkEql6Grt() 
	{
		int a = 1; int b = -1;
		if(a>b) 
			System.out.println("A is Greater "+a);
		else if(b>a) 
			System.out.println("B is Greater "+b);
		else
			System.out.println("A & B is Equal "+a+"="+b);
	}
}
