public class Test1
{	
	//static variable
	static String RAM;
	static String processor;
	
	//object variables
	int price;
	String os;
	String dtype;
	//constructer 3 args 
	public Test1(int price,String os,String dtype)
	{
		this.price=price;
		this.os=os;
		this.dtype=dtype;
	}
	public static void main(String[] args)
	{
		Test1 lenovo = new Test1(40000,"windows 11","touch");//create object lenovo variable
		Test1 dell = new Test1(50000,"ubuntu","Normal");//create object dell variable
		
		lenovo.details();//call methods multipule time->details();
		dell.details();
	
	}
	public void details()
	{
		System.out.println("laptop price ="+" "+price);
		System.out.println("laptop Os ="+" "+os);
		System.out.println("laptop type ="+" "+dtype);
	}

}
