package condition;
public class If_Condition 
{

	public static void main(String[] args)
	{
		If_Condition ifcon = new If_Condition();
		ifcon.check3Num();
	}
	private void check3Num() 
	{
		int IND = 11;
		int PAK = 10;
		int AUS = 11;
		if(IND>PAK)
		{
			if(IND>AUS)
				System.out.println("Ind is won");
			else if(AUS>IND)
				System.out.println("Aus is won");
			else
				System.out.println("Aus = Ind");
		}
		else if(PAK>IND) 
		{
			if(PAK>AUS)
				System.out.println("Pak is win");
			else if(AUS>PAK)
				System.out.println("Aus is won");
			else
				System.out.println("Pak = Aus");
		}
		else 
		{
			if(IND>AUS)
				System.out.println("IND = PAK");
			else if(AUS>IND)
				System.out.println("AUS is won");
			else
				System.out.println("IND = PAK = AUS");
		}		
	}
}
