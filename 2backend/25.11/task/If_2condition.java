package condition;
public class If_2condition 
{
	public static void main(String[] args) 
	{
		If_2condition if2con = new If_2condition();
		if2con.check1Condition();
		if2con.check2Condition();
		if2con.check3Condition();
		if2con.check4Condition();
	}
	private void check1Condition() 
	{
		int x=-1;
		if(x>0)
			System.out.println("positive");
		else if(0>x)
			System.out.println("negative");
		else
			System.out.println("0");
	}
	private void check2Condition() 
	{
		int a=-1;
		int b=1;
		
		if(a>b)
			System.out.println("a is big");
		else if(b>a)
			System.out.println("b is big");
		else
			System.out.println("a & b equal");
	}
	private void check3Condition()
	{
		int g=1; int h=1; int i=0;
		if(g>h)
		{
			if(g>i)
				System.out.println("g is big");
			else if(i>g)
				System.out.println("i is big");
			else
				System.out.println("g=i and big");
		}
		else if(h<g)
		{
			if(h>i) 
				System.out.println("h is big");
			else if(i<h)
				System.out.println("i is big");
			else 
				System.out.println("h=i and big");
		}
		else 
		{
			if(g>i) 
				System.out.println("g=h and big");
			else if(i>g)
				System.out.println("i  is big");
			else
				System.out.println("g=h=i");
		}	
	}
	private void check4Condition() 
	{
		int p=1; int q=0;
		int r=1; int s=1;
		if(p>q)
		{
			if(p>r)
			{
				if(p>s)
					System.out.println("p is big");
				else if(s>p)
					System.out.println("s is big");
				else 
					System.out.println("p & s Equal");
			}
			else if(r>p) 
			{
				if(r>s)
					System.out.println("r is big");
				else if(s>r)
					System.out.println("s is big");
				else
					System.out.println("s & r Equal");
			}
			else
			{
				if(r>s)
					System.out.println("r=p");
				else if(s>r)
					System.out.println("s is big");
				else
					System.out.println("p=r=s big");
			}
				
		}
		else if(q>p)
		{
			if(q>r)
			{
				if(q>s)
					System.out.println("q is big");
				else if(s>q)
					System.out.println("s is big");
				else
					System.out.println("q & s Equal");
			}
			else if(r>q) 
			{
				if(r>s)
					System.out.println("r is big");
				else if(s>r)
					System.out.println("s is big");
				else
					System.out.println("r & s Equal");
			}
			else 
			{
				if(r>s)
					System.out.println("r and q Equal");
				else if(s>r)
					System.out.println("s is big");
				else
					System.out.println("q=r=s big");
			}
		}
		else if(p>r) 
		{
			if(p>s)
				System.out.println("p & q Equal");
			else if(s>p)
				System.out.println("s is big");
			else
				System.out.println("p=q=s big");
		}
		else if(r>p)
		{
			if(r>s) 
				System.out.println("r is big");
			else if(s>p)
				System.out.println("s is big");
			else 
				System.out.println("r=s equal");		
		}
		else 
		{
			if(p>s)
				System.out.println("p=q=r equal big");
			else if(s>p)
				System.out.println("s is big");
			else
				System.out.println("pqrs is equal");
		}
		
	}

}
