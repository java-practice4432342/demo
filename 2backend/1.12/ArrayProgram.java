package condition;
public class ArrayProgram 
{
	public static void main(String[] args) 
	{
		ArrayProgram array = new ArrayProgram();
		array.checkTolAvg();
		array.checkLoop();
		array.checkFails();
	}
	private void checkFails() 
	{
		int[] marks = { 90, 25, 89, 10, 28 };
		int fail =0;
		int total =0;
		int count = 0;
		while(count < marks.length) 
		{
			if(marks[count] <= 35)
			{
				//System.out.print(marks[count]);
				fail = fail + 1;
			}
			total = total + marks[count] ;
			count++;
		}
		System.out.println("\n"+"fail count = "+fail);
	}
	private void checkLoop()
	{
		int[] marks = { 90, 25, 89, 100, 28 };
		int i = 0;
		while (i <= 5) 
		{
			if(i%2==0) 
			{
				System.out.print(marks[i]+" \n");
			}
			if (i % 2 == 0) 
			{
				System.out.print(i);
			}
			i = i + 1;
		}
	}
	private void checkTolAvg() 
	{
		int[] marks = { 90, 25, 89, 100, 28 };
		int total=0;
		int j =0;
		while(j<marks.length)
		{
			total = total + marks[j];
			j++;
		}
		System.out.println("total ="+total+"\n");
		System.out.println("average ="+total/marks.length+"\n");
		
	}
}
