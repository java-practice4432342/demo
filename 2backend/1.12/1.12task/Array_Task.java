package condition;

public class Array_Task {

	public static void main(String[] args) 
	{
		Array_Task at = new Array_Task();
		at.check1();
	}

	private void check1() 
	{
		int[] mark = {60,54,100,60,10};
		int total = 0;
		int index = 0;
		int fail = 0;
		int avg = 0;
		int min = 0;
		int max = 0;
		while(index< mark.length) 
		{
			System.out.print("index "+index+" = ");
			System.out.println(mark[index]);
			total = total + mark[index];
			avg = total/ mark.length;
			if(mark[index]>max) {
				max = mark[index];
			}
			if(mark[index]<max) 
			{
				min = mark[index] ;
			}
			if(mark[index]<=35)
			{
				fail = fail + 1;
			}
			index++;					
		}
		System.out.println();
		System.out.println("min ="+min);
		System.out.println("max "+max);
		System.out.println("totalMark ="+total);
		System.out.println("your avg = "+avg);
		System.out.println("your fail sub ="+fail);
		if(avg>=91 && avg<=100 && fail==0)
			System.out.print("S grade");
		else if(avg>=81 && avg<=90 && fail==0)
			System.out.println("A grade");
		else if(avg>=71 && avg<=80 && fail==0)
			System.out.println("B grade");
		else if(avg>=61 && avg<=70 && fail==0) 
			System.out.println("C grade");
		else if(avg>=51 && avg<=60 && fail==0)
			System.out.println("D grade");
		else if(avg>=50 && avg<=45 && fail==0)
			System.out.println("E grade");
		else 
			if(fail>0)
				System.out.println(fail+" fail " + " no grade");
	}

}
