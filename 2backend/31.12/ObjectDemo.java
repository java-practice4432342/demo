package offline;

public class ObjectDemo {

	public static void main(String[] args) {
		
//object is a root of class hirarchy. every class obj is super class all obj include array.import a method of class
		ObjectDemo  od = new ObjectDemo();
		ObjectDemo  od2 = new ObjectDemo();
		
		ObjectDemo  od3 = od2;
		System.out.println(od.hashCode());
		System.out.println(od2.hashCode());
		
		System.out.println(od.equals(od2));
		System.out.println(od2.equals(od3));
		
	}

}
