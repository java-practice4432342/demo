package condition;
public class Task_While 
{
	public static void main(String[] args) 
	{
		Task_While whileTask = new Task_While();
		whileTask.task1();
		whileTask.task2();
		whileTask.task3();
		whileTask.task4();
	}
	private void task1() 
	{
		int count = 0;
		while(count < 5) 
		{
			System.out.print("HI ");
			count = count + 1;
		}
	}
	private void task2() 
	{
		int count = 0;
		System.out.println("\n");
		while(count < 5)
		{
			System.out.print("HI \n");
			count = count + 1;
		}
	}
	private void task3() 
	{
		int count = 0;
		System.out.print("\n");
		while(count < 5) 
		{
			System.out.print(count+1+" ");
			count = count + 1;
		}
		
	}
	private void task4() 
	{
		int count = 0;
		System.out.print("\n\n");
		while(count < 5) 
		{
			System.out.print(count+" ");
			count = count + 1;
		}
		
	}
}
