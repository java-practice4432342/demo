package condition;
public class Pattern 
{
	public static void main(String[] args) 
	{
		Pattern pattern = new Pattern();
		pattern.check1();
		pattern.check2();
		pattern.check3();
		pattern.check4();
		
	}
	private void check4() 
	{
		int no = 5;
		System.out.println(no++ + --no+no--*++no/no--+"\n");//15
		
		int a = 0;
		a=a++ - --a + ++a - a--;
		System.out.println(a+"\n");//i=0
		
		int i=1,j=2,k=3,m;
		m=i-- -j-- -k--;
		System.out.println("i="+i);//0
		System.out.println("j="+j);//1
		System.out.println("k="+k);//2
		System.out.println("m="+m);//-4
		
	}
	private void check3() 
	{
		int row = 5;
		while(row >= 1)
		{
			int column = 1;
			while(column<=row)
			{
				System.out.print("*");
				column = column + 1;
			}
			row = row - 1;
			System.out.println();
		}
		
	}
	private void check2() 
	{
	 int row = 1;
	 while(row <= 5)
	 {
		 int column = 1;
		 while(column <= row)
		 {
			 System.out.print("*");
			 column = column + 1;
		 }
		 row = row + 1;
		 System.out.println();
	 }
	}
	private void check1()
	{
		int row = 1;
		while(row <= 5) 
		{
			int star = 1;
			while(star<=10) 
			{
				System.out.print("*");
				star++;
			}
			row++;
			System.out.println();
		}
		
	}

}
