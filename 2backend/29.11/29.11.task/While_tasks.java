package condition;
public class While_tasks {
	public static void main(String[] args)
	{
		//System.out.println((char)65);
		While_tasks taskwhile = new While_tasks();
		taskwhile.multiple();
		taskwhile.squre();
		taskwhile.task1();
		taskwhile.task2();	
		taskwhile.task3();	
		taskwhile.task4();	
	}
	private void squre() 
	{
		int sqr = 1;
		while(sqr<=10)
		{
			System.out.println(sqr+"*"+sqr+"="+sqr*sqr+"");
			sqr = sqr + 1;
		}
		System.out.println();
	}
	private void multiple() 
	{
		int no = 4;
		int multi = 1;
		while(multi<=10) 
		{
			System.out.println(multi+" * "+no+" =  "+no*multi );
			multi = multi + 1;
		}
		System.out.println();
	}
	private void task1() 
	{
		int row = 1;
		int letter = 64; 
		while(row<=5)
		{
			int column = 0;
			while(column<5)
			{
				System.out.print((char)(letter+row)+" ");
				column = column + 1;
			}
			row = row + 1;
			System.out.println();
		}
		System.out.println();
	}
	private void task2() 
	{
		int askey = 65;
		int row = 5;
		while(row>=1)
		{
			int column = 0;
			while(column<5)
			{
				System.out.print((char)(askey+column)+" ");
				column = column + 1;
			}
			row = row - 1;
			System.out.println();
		}
		System.out.println();
	}
	private void task3() 
	{
		int No = 1;
		while(No <= 20)
		{
			System.out.print(No + " ");
			if(No%5 == 0)
			{
				System.out.println();
			}
			No = No + 1;
		}
		System.out.println();
	}
	private void task4() 
	{
		int No = 20;
		while(No >= 1)
		{
			System.out.print(No +" ");
			if(No% 5 == 1)
			{
				System.out.println();
			}
			No = No - 1;
		}
		System.out.println();
	}

}
