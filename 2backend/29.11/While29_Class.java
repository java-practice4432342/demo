package condition;

public class While29_Class {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		While29_Class day29= new While29_Class();
		day29.check6Task();
		day29.check7Task();
		day29.check8Task();
	}
	private void check8Task() {
		// TODO Auto-generated method stub
		int row = 1;
		while(row<=5)
		{
			int coulumn = 1;
			while(coulumn<=5) {
				System.out.print(1);
				coulumn = coulumn + 1;
			}
			row = coulumn + 1 ;
			System.out.println();
		}
		System.out.println();
	}

	private void check7Task() {
		// TODO Auto-generated method stub
		int row = 5;
		while(row>=1)
		{
			int coulumn = 5;
			while(coulumn>=1) {
				System.out.print(coulumn);
				coulumn = coulumn - 1;
			}
			row = row - 1 ;
			System.out.println();
		}
		System.out.println();
	}

	private void check6Task() 
	{
		int row = 5;
		while(row>=1)
		{
			int coulumn = 1;
			while(coulumn<=5) {
				System.out.print(row);
				coulumn = coulumn + 1;
			}
			row = row - 1 ;
			System.out.println();
		}
		System.out.println();
	}
}
