package offline;

public class TypeCasting {

	public static void main(String[] args) 
	{
		float price = 103.07f;
		int price2 = (int)price;
		System.out.println(price2);
		
		float price3 = (float)price2;
		System.out.println(price3);
		
		char ch ='A';
		int no = (int)ch;
		System.out.println(no);
	}

}

