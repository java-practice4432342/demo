package offline;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Mapdemo {

	public static void main(String[] args) 
	{
//	HashMap hm = new HashMap();
//	LinkedHashMap hm = new LinkedHashMap();
	TreeMap hm = new TreeMap();
	hm.put("iddly", 30);
	hm.put("dosai", 40);
	hm.put("pongal", 40);
	hm.put("vadacarry", 50);
	hm.put("curd", 10);
	
	System.out.println(hm);
	System.out.println(hm.get("pongal"));
	System.out.println(hm.containsKey("curd"));
	
	System.out.println(hm.containsValue(40));
	System.out.println(hm.remove("curd"));
	System.out.println(hm);
	
	System.out.println(hm.replace("vadacarry", 50, 40));
	System.out.println(hm);
	System.out.println(hm.entrySet());
	System.out.println(hm.keySet());
	System.out.println(hm.values());
	
	Set s = hm.entrySet();
	
	Iterator  i = s.iterator();
	while(i.hasNext())
	{
		//System.out.println(i.next());
		Map.Entry me = (Map.Entry)i.next();//cursor pointer
		System.out.println(me.getKey()+"cost "+me.getValue());
		if(me.getKey().equals("dosai")) {
			me.setValue(34);
		}
		me.setValue((int)me.getValue()+5);
		
	}
	System.out.println(hm);
	}

}
