package condition;

public class StringMethods 
{
	public static void main(String[] args) 
	{
		StringMethods smethod = new StringMethods();
		smethod.checks();
	}
	private void checks()
	{
		String name ="sarath";
		String name2 = new String("Sarath");
		System.out.println(name.charAt(0));
		System.out.println(name.equals(name2));
		//System.out.println(name==name2);
		System.out.println(name.contains("sar"));
		System.out.println(name.compareToIgnoreCase(name2));
		System.out.println(name.equalsIgnoreCase(name2));
		System.out.println(name.hashCode());
		System.out.println(name2.hashCode());
		System.out.println(name.startsWith("S"));
		System.out.println(name.endsWith("h"));
		String name3 = "Raja";
		System.out.println(name3.isEmpty());
		System.out.println(name3.indexOf("R"));
		System.out.println(name3.lastIndexOf("a"));
		
		
	}
	

}
