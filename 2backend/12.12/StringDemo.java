package condition;

public class StringDemo {

	public static void main(String[] args) 
	{
		StringDemo sdemo = new StringDemo();
		//sdemo.check1();
		//sdemo.check2();
		//sdemo.check3();
	}

	private void check3() 
	{
		String name ="sarathkumar";
		int i = 0;
		int alpha =0;
		while(i<name.length())
		{
			System.out.println(name.charAt(i));
			if(name.charAt(i)=='a') 
			{
				
			}
			i++;
		}
		
	}

	private void check2() 
	{
		// TODO Auto-generated method stub
		String name = "sarathkumar";
		System.out.println(name.length());
		int i = 0;
		int count = 0;
		while(i<name.length()) 
		{
			if(name.charAt(i)=='a') {
				System.out.println(name.charAt(i));
				count++;
			}
			i++;
		}
		System.out.println("count "+count);

		
	}

	private void check1()
	{
		String p1 = "china";//this is create scp
		System.out.println(System.identityHashCode(p1));
		String p2 = "india";
		System.out.println(System.identityHashCode(p2));
		String p3 = "india";
		System.out.println(System.identityHashCode(p3));
		
		String p4 = new String("india");//new create heap memory
		System.out.println(System.identityHashCode(p4));
		String p5 = "india";
		System.out.println(System.identityHashCode(p5));
	}

}
