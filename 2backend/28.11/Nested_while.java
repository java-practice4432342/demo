package condition;

public class Nested_while 
{
	public static void main(String[] args) 
	{
		Nested_while nest_while = new Nested_while();
		nest_while.chec2line();
		nest_while.chec3line();
		nest_while.chec5line();
	}
	private void chec2line() 
	{
		int line = 0;
		while(line < 2)
		{
			int count = 0; 
			while(count < 5)
			{
				System.out.print(1+" ");
				count = count + 1;
			}
			line = line + 1;
			System.out.println(" ");
		}
		
	}
	private void chec3line() 
	{
		int l = 1;
		while(l<=3)
		{
			int c = 1;
			while(c<=3)
			{
				System.out.print("hi ");
				c = c + 1 ;
			}
			l = l + 1;
			System.out.println("");
		}
	}
	private void chec5line() 
	{
		int row = 0;
		while(row < 5)
		{
			int column = 0;
			while(column < 5) 
			{
				System.out.print(1 + " ");
				column = column + 1;
			}
			row = row + 1;
			System.out.print("\n");
		}
	}


}
