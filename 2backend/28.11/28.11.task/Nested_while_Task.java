package condition;

public class Nested_while_Task {

	public static void main(String[] args) 
	{	
		Nested_while_Task NestTask = new Nested_while_Task();
		NestTask.check1Task();
		NestTask.check2Task();
		NestTask.check3Task();
		NestTask.check4Task();
		NestTask.check5Task();
	}

	private void check1Task() 
	{
		int line = 0;
		while(line < 5)
		{
			int count = 0;
			while(count < 5)
			{
				System.out.print(count+1+" ");
				count = count + 1;
			}
			line = line +1 ;
			System.out.println("");
		}
		System.out.print("\n");
	}
	private void check2Task() 
	{
		int row = 1;
		while(row<=5)
		{
			int colunm = 1;
			while(colunm <= 5)
			{
				System.out.print("A ");
				colunm = colunm + 1;
			}
			System.out.println("");
			row = row + 1;
		}
		System.out.print("\n");
	}
	private void check3Task() 
	{	
		int cNum = 1;
			while(cNum <= 5)
			{
				System.out.print(cNum*cNum+" ");
				cNum = cNum + 1;
			}
			System.out.println("\n");
	}
	
	private void check4Task() 
	{
		int row = 1;
		while(row <= 5)
		{
			int col = 1;
			while(col <= 5)
			{
				System.out.print(col*2+" ");
				col = col + 1;
			}
			row = row + 1;
			System.out.println("\n");
		}
		
	}
	public void check5Task() 
	{
		int row = 1;
		while(row <= 5)
		{
			int no = 1;
			while(no <= 5) 
			{
				System.out.print(no*row+" ");
				no = no + 1;
			}
			row = row + 1;
			System.out.println("");
		}
		
	}

}
