package condition;

import java.util.Scanner;

public class ExceptionDemo {

	public static void main(String[] args) 
	{
		ExceptionDemo ed = new ExceptionDemo();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter numbers:");
		int n1 = scan.nextInt();
		int n2 = scan.nextInt();
		ed.divide(n1,n2);
		ed.subtract(n1,n2);
	}

	private void divide(int n1, int n2) 
	{
		try {//exp posible area
			System.out.println(n1/n2);
		}
		catch(ArithmeticException ae) {//exp handling
			System.out.println("check n2");
			ae.printStackTrace();
		}
		//checked exp = complie time exp
		//unchecked exp = run time exp
		System.out.println(n1/n2);
	}

	private void subtract(int n1, int n2) {
		System.out.println(n1-n2);
		
	}

}
