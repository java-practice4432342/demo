package learnThreads;

public class ThreadDemo{

	public static void main(String[] args) {
		
		ThreadChild tc = new ThreadChild();	
		//tc.run();
		tc.start();
		System.out.println(tc.getId());
		System.out.println(tc.getName());
		System.out.println(tc.getPriority());
		System.out.println(tc.isAlive());
		System.out.println(tc.isDaemon());
		
		ThreadChild tc2 = new ThreadChild();
		tc2.start();
		System.out.println(tc2.getId());
		System.out.println(tc2.getName());
		System.out.println(tc2.getPriority());
		System.out.println(tc2.isAlive());
		System.out.println(tc2.isDaemon());
		ThreadDemo.loop();
		
	}
	public static void loop() {
		for(int i=1;i<=5;i++) {
			System.out.println("ThreadDemo "+i);
		}
		
	}

}
