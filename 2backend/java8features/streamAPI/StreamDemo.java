package streamAPI;

import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.stream.IntStream;

public class StreamDemo {

	public static void main(String[] args) 
	{	
		int[] ar = {10,20,30,40,50};
		System.out.println(ar.length);
		
		IntStream s = Arrays.stream(ar);
		long c =s.count();
		System.out.println(c);
		
		IntStream s1 = Arrays.stream(ar);
		
		//s1 = s1.sorted();
		//s1.forEach(System.out::print);
		//s1.forEach(var->System.out.print(var+" "));
		
		//Arrays.stream(ar).sorted().forEach(System.out::print);
		
		//==============================================================
		
//		OptionalDouble od = Arrays
//				.stream(ar)//intermediate operations
//				.average();//intermediate operations
//				System.out.println(od.isPresent());
//				System.out.println(od.getAsDouble());
	
		long od = Arrays
				.stream(ar)
				.count();
		System.out.println(od);
		
		

//				----count------
//				
//				long od = Arrays
//				  .stream(ar)//intermediate operations
//				  .count();//intermediate operations
//				  //.forEach(System.out::println);//Terminal operations
//				  System.out.println(od);
//
//				
//				-----average------
//				
//				OptionalDouble od = Arrays
//				  .stream(ar)//intermediate operations
//				  .average();//intermediate operations
//				  //.forEach(System.out::println);//Terminal operations
//				  System.out.println(od.isPresent());
//				  System.out.println(od.getAsDouble());
//
//			
//				max
//
//				
//				OptionalInt od = Arrays
//				  .stream(ar)//intermediate operations
//				  .max();//intermediate operations
//				  //.forEach(System.out::println);//Terminal operations
//				  System.out.println(od.isPresent());
//				  System.out.println(od.getAsInt());
//
//				
//				min
//
//				
//				OptionalInt od = Arrays
//				  .stream(ar)//intermediate operations
//				  .min();//intermediate operations
//				  //.forEach(System.out::println);//Terminal operations
//				  System.out.println(od.isPresent());
//				  System.out.println(od.getAsInt());
//
//				
//				distinct
//
//				
//				Arrays
//				  .stream(ar)//intermediate operations
//				  .distinct()
//				  .forEach(System.out::print);
//
//				
//				findFirst
//
//				
//				OptionalInt od = Arrays
//				  .stream(ar)//intermediate operations
//				  .findFirst();//intermediate operations
//				  //.forEach(System.out::println);//Terminal operations
//				  System.out.println(od.isPresent());
//				  System.out.println(od.getAsInt());
//
//				
//				findAny
//
//				
//				OptionalInt od = Arrays
//				  .stream(ar)//intermediate operations
//				  .findAny();//intermediate operations
//				  //.forEach(System.out::println);//Terminal operations
//				  System.out.println(od.isPresent());
//				  System.out.println(od.getAsInt());
	}
}
