package streamAPI;

import java.util.ArrayList;

public class Streams {

	public static void main(String[] args) {
		
		Employees emp1 = new Employees(1, "selva", 2);
		Employees emp2 = new Employees(1, "sarath", 1);
		Employees emp3 = new Employees(1, "kumar", 4);
		
		ArrayList<Employees> al = new ArrayList<Employees>();
		al.add(emp3);
		al.add(emp2);
		al.add(emp1);
		
		al.stream()
		.map(na->na.getName())
		.forEach(System.out::println);
		System.out.println("------------------------------------");
		
		al.stream()
		.filter(na->na.getExp()>1)
		.map(nam->nam.getName())
		.forEach(System.out::println);
	}

}
