package streamAPI;

import java.util.ArrayList;
import java.util.Arrays;

public class Streamdemo2 {

	public static void main(String[] args) {
		int[] ar= {100,20,40,30,53,10};
		
//		Arrays.stream(ar)
//		.skip(2)
//		.forEach(System.out::println);
		
		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(10);
		al.add(20);
		al.add(30);
		al.add(20);
		
		System.out.println(al);
		
		al.stream()
		.distinct()
		.forEach(System.out::println);
		
		System.out.println("=====================================");
		
		al.stream()
		.skip(1)
		.forEach(System.out::println);
		
		System.out.println("=====================================");
		
		al.stream()//10,20,30,20
		.limit(2)
		.skip(1)//10,20
		.forEach(System.out::println);
		
		
	}

}
