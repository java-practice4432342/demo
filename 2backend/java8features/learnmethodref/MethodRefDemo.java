package learnmethodref;

public class MethodRefDemo {
	
	public MethodRefDemo(int no){
		System.out.println("const =>"+no);
	}
	
	public static void main(String[] args) 
	{
		//double collean optator
//		ContractRef c = System.out::println;
//		c.display(33);
		
		//MethodRefDemo md = new MethodRefDemo();
		//md.show(11);
		//ContractRef c = md::show;
		//c.display(35);//non-static methods calling
		//c.display(100);
		//ContractRef cc = MethodRefDemo::show;
//		ContractRef ccc = new MethodRefDemo()::show;
//		ccc.display(55);
		
		ContractRef c = MethodRefDemo::new;
		c.display(312);
		
	}

	private void show(int i) {
		System.out.println("show ->"+i);
		
	}
	
}
