package learnmethodref;

@FunctionalInterface
public interface ContractRef {

	public void display(int no);
}
