package java8features;

public class Test2 {
	public static void main(String[] args) {
		
		
		Rule2 ru = () -> System.out.println("interface reference");
		ru.add();
		
		Test2 te = new Test2();
		te.add();
		
		Rule3 rule3 = (no1,no2)->System.out.println(no1+no2);
		rule3.total(10, 20);
		
		Rule4 rule4 = (a1,a2)->{return a1+a2;};
		System.out.println(rule4.addtion(100, 200));
		
		
		
	}

	private void add() {
	System.out.println("test 2 add method");
		
	}
}
