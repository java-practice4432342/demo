package default_static;

public interface Contract 
{
	public default void calculate(int no1,int no2) {
		System.out.println(no1+no2);
	}
	public static void add() {
		System.out.println("add method 1");
	}
}
