package default_static;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
public class PredicateDemo2
{
	 public static void main(String[] args) 
	 {
	//  Predicate<Integer> p = marks -> (marks>=35);
	//  System.out.println(p.test(36));

	//  Predicate<String> p = arg->(arg.length()>5); 
	//  System.out.println(p.test("selva"));

	  

	//  ArrayList al = new ArrayList();
	//  al.add("h");
	//
	//  Predicate<Collection> p2 = (al2) -> (al2.size() > 0);
	//  System.out.println(p2.test(al));

	//  PedicateDemo pd = new PedicateDemo();
	//  System.out.println(pd.check(45));
	  int[] ar = { 10, 30, 34, 20, 67, 56,11 };
	  Predicate<Integer> p = (no) -> (no % 2 == 0);
	  Predicate<Integer> p2 = (no) -> (no>30);

	  PredicateDemo2 pd = new PredicateDemo2();
	  //pd.check(p,ar);
	  //pd.check(p.negate(), ar);
	  //pd.check(p2.and(p), ar);
	  //pd.check(p2.or(p), ar);
	  
	  Function<String, Integer> f = (str)->str.length();
	  System.out.println(f.apply("sarathkumar"));
	  
	//	  public int apply(String str) {
	//			 return str.length();
	//		 };
	  
	  ArrayList<Integer> al = new ArrayList<Integer>();
	  al.add(10);
	  al.add(20);
	  al.add(30);
	  System.out.println(al);
	  //Eternal looping 
//	  for (Integer num : al) 
//	  {
//		System.out.println(num);
//	  }
	  
	  al.forEach(num->System.out.print(num+" "));
	  Consumer<Integer> c = (n)->System.out.println("\n"+n);
	  c.accept(33);
	  
	  
	 }
	 
	 private void check(Predicate<Integer> p, int[] ar) {
	  for (int i = 0; i < ar.length; i++) {
	   System.out.println(p.test(ar[i]));
	  }
	  
	  
	 }
	 
	 
	// public boolean check(int marks) {
	//  if(marks>=35) {
	//   return true;
	//  }
	//  else
	//   return false;
	// }

}
