package default_static;

public class Employee implements Contract,Contract2{

	public static void main(String[] args) 
	{
		Contract c = new Employee();
		c.calculate(10, 20);
		Contract.add();
		Contract2.add();
	}

	@Override
	public void calculate(int no1, int no2) {
		
		Contract2.super.calculate(no1, no2);
		Contract.super.calculate(no1, no2);
	}

}
