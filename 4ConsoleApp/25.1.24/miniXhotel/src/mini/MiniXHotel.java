package mini;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MiniXHotel {

	public static void main(String[] args) throws SQLException 
	{
		MiniXHotel minix = new MiniXHotel();
		//minix.create();
		minix.read();
		//minix.update();
		minix.delete();

	}
	private void create() throws SQLException {
		String url = "jdbc:postgresql://localhost:5432/mini_xhotel";
		String dbname = "postgres";
		String psw = "sarath";
		String qry = "insert into hotel_menu values(?,?,?)";
		
		Connection con = DriverManager.getConnection(url, dbname, psw);
		PreparedStatement st = con.prepareStatement(qry);
		st.setInt(1,2);
		st.setString(2, "MuttonRice");
		st.setInt(3, 200);
		int rows = st.executeUpdate();
		System.out.println(rows+" no of rows affected");
		
		con.close();
	}
	private void read() throws SQLException 
	{
		String url = "jdbc:postgresql://localhost:5432/mini_xhotel";
		String dbname = "postgres";
		String pass = "sarath";
		String qry = "select * from hotel_menu";
		Connection	con = DriverManager.getConnection(url,dbname,pass);
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(qry);
		
		while(rs.next()) {
			System.out.println(rs.getInt(1));
			System.out.println(rs.getString(2));
			System.out.println(rs.getInt(3));
		}
		
		con.close();
	}
	private void update() throws SQLException {
		String url = "jdbc:postgresql://localhost:5432/mini_xhotel";
		String dbname = "postgres";
		String pass ="sarath";
		String qty = "update hotel_menu set price = ? where id = ?";
		
		Connection con = DriverManager.getConnection(url, dbname, pass);
		PreparedStatement ps =con.prepareStatement(qty);
		ps.setInt(1, 120);
		ps.setInt(2, 1);
		int rows = ps.executeUpdate();
		System.out.println(rows+" row updated");
		con.close();
	}
	private void delete() throws SQLException {
		String url ="jdbc:postgresql://localhost:5432/mini_xhotel";
		String dbname = "postgres";
		String pass = "sarath";
		String qty = "delete from hotel_menu where id = ?";
		Connection con = DriverManager.getConnection(url, dbname, pass);
		PreparedStatement ps= con.prepareStatement(qty);
		ps.setInt(1, 2);
		int rows = ps.executeUpdate();
		System.out.println(rows+" row deleted");
		con.close();
	}

}
