package consoleApp;

import java.util.Scanner;

//main class
public class MiniXBank extends BankAccount {
    public MiniXBank(String cname, int no) {
        super(cname, no);
    }

    public static void main(String[] args) {
        MiniXBank mxbank = new MiniXBank("Sarath", 12345);
        mxbank.customerMenu();
    }
}

// Account for customer
class BankAccount {
    private int balance;
    private int preTransaction;
    private String cname;
    private int accNo;

    public BankAccount(String name, int cid) {
        this.cname = name;
        this.accNo = cid;
    }

    protected void deposit(int amt) {
        if (amt != 0) {
            balance += amt;
            preTransaction = amt;
        }
    }

    protected void withdraw(int amt) {
        if (amt != 0 && amt <= balance) {
            balance -= amt;
            preTransaction = -amt;
        } else {
            System.out.println("\t No balance...deposit first");
        }
    }

    protected void getPreviousTransaction() {
        if (preTransaction > 0) {
            System.out.println("\t Deposit Amount : " + preTransaction);
        } else if (preTransaction < 0) {
            System.out.println("\t Withdraw Amount : " + Math.abs(preTransaction));
        } else {
            System.out.println("\t No Transaction");
        }
    }

    protected void customerMenu() {
        int option;
        Scanner scan = new Scanner(System.in);
        System.out.println("\n******************************************************\n");
        System.out.println("\t Welcome : " + cname);
        System.out.println("\t Acc No  : " + accNo);
        System.out.println("\n******************************************************\n");
        System.out.println("\t ~~~ MiniX Bank ~~~");
        System.out.println("\t 1. Check Balance");
        System.out.println("\t 2. Deposit");
        System.out.println("\t 3. Withdraw");
        System.out.println("\t 4. Last Transaction");
        System.out.println("\t 5. Exit \n");

        do {
            System.out.println("\n******************************************************\n");
            System.out.print("Enter Input Number : ");
            option = scan.nextInt();
            switch (option) {
                case 1:
                    System.out.println("\n******************************************************\n");
                    System.out.println("\t Account Balance = " + balance);
                    break;
                case 2:
                    System.out.println("\n******************************************************\n");
                    System.out.print("\t Enter Deposit Amount : ");
                    int amt1 = scan.nextInt();
                    deposit(amt1);
                    System.out.println("\t Account Balance = " + balance);
                    break;
                case 3:
                    System.out.println("\n******************************************************\n");
                    System.out.print("\t Enter Withdraw Amount : ");
                    int amt2 = scan.nextInt();
                    withdraw(amt2);
                    System.out.println("\t Withdraw Successfully " + amt2);
                    System.out.println("\t Account Balance = " + balance);
                    break;
                case 4:
                    System.out.println("\n******************************************************\n");
                    getPreviousTransaction();
                    break;
                case 5:
                    System.out.println("\n******************************************************\n");
                    System.out.println("\t Thank you for using MiniX Bank service");
                    break;
                default:
                    System.out.println("\t Invalid Option!...please enter again!");
                    break;
            }
        } while (option != 5);
    }
}
