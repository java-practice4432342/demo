package consoleApp;

import java.util.Scanner;

public class OrderSystem 
{
	public static Scanner scan = new  Scanner(System.in);
	public static String again;
	public static int choose,quantity;
	public static double total,pay;
	public static void menu() 
	{
		System.out.println();
		System.out.println("\t\t ################### HotelMiniX Project ###################");
		System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
		System.out.println("\t\t                        Non-Veg Food Menu           \n");
		System.out.println("\t\t            1.chicken biryani       RS.150  ");
		System.out.println("\t\t            2.mutton biryani        RS.250  ");
		System.out.println("\t\t            3.chicken Rice          RS.120  ");
		System.out.println("\t\t            4.egg rice               RS.80  ");
		System.out.println("\t\t            5.cancel                        \n");
		System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
	}
	public static void order() 
	{
		System.out.print("\t\t press you want to buy? ");
		choose = scan.nextInt();
		//condition
		if(choose==1) 
		{
			System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
			System.out.println("\t\t You choose chicken biryani \n");
			System.out.print("\t\t how many chicken biryani you want to buy? ");
			quantity = scan.nextInt();
			
			total = total +(quantity*150);
			System.out.println("\t\t You want to buy again?");
			System.out.print("\t\t press Y for `Yes` or `No` for N ");
			again = scan.next();
			System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
			if(again.equalsIgnoreCase("Y")) 
			{
				order();// call the method to buy
				
			}
			else 
			{
				System.out.println("\t\t\t\t Total price "+ total);
				System.out.print("\t\t\t\t Enter a payment ");
				pay = scan.nextDouble();
				if(pay>=total) 
				{
					total = pay-total;
					System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
					System.out.println("\t\t\t\t Return Amount "+total);
				}
				else {
					System.out.println("\t\t\t\t Not Enough Payment");
				}
					
			}
		}
		else if(choose==2) 
		{
			System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
			System.out.println("\t\t You choose mutton biryani ");
			System.out.print("\t\t how many mutton biryani you want to buy? ");
			quantity = scan.nextInt();
			
			total = total +(quantity*250);
			System.out.println("\t\t You want to buy again?");
			System.out.print("\t\t press Y for `Yes` or `No` for N ");
			again = scan.next();
			
			if(again.equalsIgnoreCase("Y")) {
				order();// call the method to buy
				
			}
			else 
			{
				System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
				System.out.println("\t\t\t\t Total price "+ total);
				System.out.print("\t\t\t\t Enter a payment ");
				pay = scan.nextDouble();
				if(pay>=total) 
				{
					total = pay-total;
					System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
					System.out.println("\t\t\t\t Return Amount "+total);
				}
				else 
					System.out.println("\t\t\t\t Not Enough Payment");
			}

		}
		else if(choose==3) 
		{
			System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
			System.out.println("\t\t You choose chicken Rice ");
			System.out.print("\t\t how many chicken Rice  you want to buy? ");
			quantity = scan.nextInt();
			
			total = total +(quantity*120);
			System.out.println("\t\t You want to buy again?");
			System.out.print("\t\t press Y for `Yes` or `No` for N ");
			again = scan.next();
			System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
			if(again.equalsIgnoreCase("Y")) {
				order();// call the method to buy
				
			}
			else 
			{
				System.out.println("\t\t\t\t Total price "+ total);
				System.out.print("\t\t\t\t Enter a payment ");
				pay = scan.nextDouble();
				if(pay>=total) 
				{
					total = pay-total;
					System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
					System.out.println("\t\t\t\t Return Amount "+total);
				}
				else 
					System.out.println("\t\t\t\t Not Enough Payment");
			}

		}
		else if(choose==4)
		{
			System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
			System.out.println("\t\t You choose egg rice ");
			System.out.print("\t\t how many egg rice you want to buy? ");
			quantity = scan.nextInt();
			
			total = total +(quantity*80);
			System.out.println("\t\t You want to buy again?");
			System.out.print("\t\t press Y for `Yes` or `No` for N ");
			again = scan.next();
			System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
			if(again.equalsIgnoreCase("Y")) {
				order();// call the method to buy
				
			}
			else 
			{
				System.out.println("\t\t\t\t Total price "+ total);
				System.out.print("\t\t\t\t Enter a payment ");
				pay = scan.nextDouble();
				if(pay>=total) 
				{
					total = pay-total;
					System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
					System.out.println("\t\t\t\t Return Amount "+total);
				}
				else 
					System.out.println("\t\t\t\t Not Enough Payment");
			}

		}
		else if(choose==5)
		{
			System.out.println("\t\t +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ ");
			System.out.println("\t\t\t thank you to visit! ");
			System.exit(0);//close program
		}
		else {
			System.out.println("\t\t\t Enter 1 to 5  only! ");
			order();
		}
		
	}
	public static void main(String[] args) 
	{
		menu();
		order();//try to run

	}

}
