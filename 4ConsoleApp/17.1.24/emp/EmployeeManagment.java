package emp;

import java.util.Scanner;

public class EmployeeManagment 
{
	EmployeeService service = new EmployeeService();
	static boolean ordering = true;
	public static void menu() {
		System.out.println("\n~~~~~~~~~~~ Welcome to EmployeeManagement System ~~~~~~~~~\n");
		System.out.println("\t\t 1.Add Employee");
		System.out.println("\t\t 2.View Employee");
		System.out.println("\t\t 3.Update Employee");
		System.out.println("\t\t 4.Delete Employee");
		System.out.println("\t\t 5.View All Employee");
		System.out.println("\t\t 6.Exit \n");
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static void main(String[] args) 
	{
		try (Scanner scan = new Scanner(System.in)) //remove the yellow alert
		{
			EmployeeService service = new EmployeeService();
			
			do 
			{
				menu();//call method details
				System.out.print("\t\t Enter a choice ");
				int choice = scan.nextInt();
				switch (choice) 
				{
				case 1:
					System.out.println("\t\t Add Employee ");
					service.addEmp();
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
					break;
				case 2:
					System.out.println("\t\t View Employee ");
					service.viewEmp();
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
					break;
				case 3:
					System.out.println("\t\t Update Employee ");
					service.updateEmp();
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
					break;
				case 4:
					System.out.println("\t\t Delete Employee ");
					service.deleteEmp();
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
					break;
				case 5:
					System.out.println("\t\t View All Employee ");
					service.viewAllEmps();
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
					break;
				case 6:
					System.out.println("\t\t Thank you for using Application ");
					System.exit(0);
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
					break;

				default:
					System.out.println("\t\t Please Enter Valid Choise ");
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
					break;
				}
				
			} while (ordering);
		}
	}
}
