package emp;

import java.util.HashSet;
import java.util.Scanner;

public class EmployeeService 
{
	
	HashSet<Employee> empset = new HashSet<Employee>();
	
	Employee emp1 = new Employee(101, "Sarath" , 24 , "Developer" , "IT" , 25000);
	Employee emp2 = new Employee(102, "kumari" , 26 , "Tester" , "CO" , 20000);
	Employee emp3 = new Employee(103, "Vijay" , 30 , "DevOps Engg" , "Admin" , 30000);
	Employee emp4 = new Employee(104, "RamKumar" , 24 , "System Engg" , "CO" , 25000);
	
	Scanner scan = new Scanner(System.in);
	boolean found = false;
	
	int id;
	String name;
	int age;
	String designation;
	String department;
	double sal;
	
	public EmployeeService() 
	{
		empset.add(emp1);
		empset.add(emp2);
		empset.add(emp3);
		empset.add(emp4);
	}
	//Add emp list
	public void addEmp() 
	{
		System.out.print("\t\t Enter id: ");
		id = scan.nextInt();
		System.out.print("\t\t Enter Name ");
		name =scan.next();
		System.out.print("\t\t Enter age ");
		age = scan.nextInt();
		System.out.print("\t\t Enter Designation ");
		designation = scan.next();
		System.out.print("\t\t Enter Department ");
		department = scan.next();
		System.out.print("\t\t Enter Salary ");
		sal = scan.nextDouble();
		
		Employee emp = new Employee(id, name, age, designation, department, sal);
		empset.add(emp);
		System.out.println(emp);
		System.out.println("\t\t Employee added Successfully ");
		
	}
	//view all emp
	public void viewAllEmps() 
	{
		for(Employee emp:empset) 
		{
			System.out.println(emp);
		}
	}
	//view emp base on there Id
	public void viewEmp()
	{
		
		System.out.print("\t\t Enter Id: ");
		id = scan.nextInt();
		for(Employee emp:empset)
		{
			if(emp.getId()==id) 
			{
				found =true;
				System.out.println(emp);
			}
		}
		if(!found)
		{
			System.out.println("\t\t Employee Not Found ");
		}
	}
	//update Emp 
	public void updateEmp() 
	{
		System.out.print("\t\t Enter Id: ");
		id = scan.nextInt();
		found = false;
		for(Employee emp:empset) 
		{
			if(emp.getId()==id)
			{
				System.out.print("\t\t Enter department: ");
				department = scan.next();
				emp.setDepartment(department);
				System.out.print("\t\t Enter new salary ");
				sal = scan.nextDouble();
				emp.setSalary(sal);
				System.out.println("\t\t Updated Details of employee are: ");
				System.out.println(emp);
				found = true;
			}
		}
		if(!found)
		{
			System.out.println("\t\t Employee is not Present");
		}
		else 
		{
			System.out.println("\t\t Empolyee detaill update successfully ");
		}
	}
	//delete emp 
	public void deleteEmp() 
	{
		System.out.print("\t\t Enter id: ");
		id = scan.nextInt();
		Employee empdel = null;
		found =false;
		for(Employee emp:empset) 
		{
			if(emp.getId()==id) 
			{
				empdel = emp;
				found =true;
			}
			
		}
		if(!found) 
		{
			System.out.println("\t\t Employee is not present");
		}
		else 
		{
			empset.remove(empdel);
			System.out.println("\t\t Employeee deleted Sucessfully");
		}
	}
}
