package busReservation;

import java.util.ArrayList;
import java.util.Scanner;

public class BusMain {

	public static void main(String[] args) 
	{
		//BusDetail buses[] = new BusDetail[50];//ArrayList collection
		ArrayList<BusDetail> buses = new ArrayList<BusDetail>();
		ArrayList<BookingDetail> bookingList = new ArrayList<BookingDetail>();
		
		buses.add(new BusDetail(1, true, 2));	
		buses.add(new BusDetail(2, false, 3));	
		buses.add(new BusDetail(3, true, 2));	
		
		int userOpt = 1;
		Scanner scan = new Scanner(System.in);
		
		for (BusDetail b : buses) {//foreach loop
			b.displayBusInfo();
		}
		
		while(userOpt==1) 
		{
			System.out.println("Enter  1 to Book and 2 to exit");
			userOpt = scan.nextInt();
			if(userOpt==1) 
			{
				BookingDetail booking = new BookingDetail();
				if(booking.isAvailable(bookingList,buses)) 
				{
					bookingList.add(booking);
					System.out.println("your booking is conformed ");
				}
				else 
					System.out.println("Sorry, bus is full, try another bus or date. ");
			}
		}
	}

}
