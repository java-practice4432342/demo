package busReservation;
import java.text.ParseException;
import java.text.SimpleDateFormat;  
import java.util.*;
public class BookingDetail 
{
	String passengerName;
	int busNo;
	Date date;//date object
	
	public BookingDetail() 
	{
		Scanner scan = new Scanner(System.in);
	
			System.out.println("Enter name of Passenger: ");
			passengerName = scan.next();
			System.out.println("Enter Bus No: ");
			busNo = scan.nextInt();
			System.out.println("Enter date dd-mm-yyyy");
			String DateInput = scan.next();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
			
			try
			{
				date = dateFormat.parse(DateInput);
			} catch (ParseException e) 
			{
				e.printStackTrace();
			}
	}
	
	public boolean isAvailable(ArrayList<BookingDetail> bookingList, ArrayList<BusDetail> buses)
	{
		int capacity = 0;
		for(BusDetail bus : buses) 
		{
			if(bus.getBusNo()==busNo) 
			{
				capacity = bus.getCapacity();
			}
		}
		int booked = 0;
		for (BookingDetail book : bookingList)
		{
			if(book.busNo == busNo && book.date.equals(date))
			{
				booked++;
			}
		}
		return booked < capacity?true:false;
	}

}
