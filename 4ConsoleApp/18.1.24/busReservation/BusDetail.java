package busReservation;

public class BusDetail 
{
	//instance var
	private int busNo ;
	private boolean ac;
	private int capacity;//get and set use data get-gave, modify-set
	
	public BusDetail(int no,boolean ac,int capacity) 
	{
		this.busNo = no;
		this.ac = ac;
		this.capacity = capacity;
	}

	public int getBusNo() {
		return busNo;
	}

	public void setBusNo(int busNo) {
		this.busNo = busNo;
	}

	public boolean isAc() {
		return ac;
	}

	public void setAc(boolean ac) {
		this.ac = ac;
	}

	public int getCapacity() {//accessor method
		return capacity;
	}

	public void setCapacity(int capacity) {//mutator
		this.capacity = capacity;
	}
	
	public void displayBusInfo() {
		System.out.println("Bus No:" +busNo+ " Ac : "+ ac +" Total capacity : "+capacity);
	}
	
}
