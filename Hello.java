public class Hello
{
    //global (static) variables.
    static String name = "Veeraiya";
    static int age = 15;

	public static void main(String[] args)
	{
        //local variables.
         String name = "sarath";
            int age =28;

        //this is access a class variable.
        System.out.println(Hello.name);
        System.out.println(Hello.age);

        //this is access a local variable.
        System.out.println(name);
        System.out.println(age);

	    //this is words or strings
		 System.out.println("hi sarath");
            
	}

}
