package collection;

import java.util.HashSet;

public class HashSetDemo {

	public static void main(String[] args) {
		HashSet hs = new HashSet();
		hs.add(5);
		hs.add(true);
		hs.add(10.5f);
		hs.add("hi");
		hs.add("bye");
		System.out.println(hs.equals(hs));
		System.out.println(hs);
	}

}
