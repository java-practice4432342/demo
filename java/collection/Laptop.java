package collection;

public class Laptop {
	
	int price;
	int ram;
	boolean touchscreen;
	String model;

	public String toString() {
		return "LaptopList : "+this.price+" "+this.ram+" "+this.touchscreen+" "+model;
	}
	public Laptop() {
		
	}

	public Laptop(int i, int j, boolean b,String m) {
		this.price = i;
		this.ram = j;
		this.touchscreen = b;
		this.model = m;
		
	}

	public static void main(String[] args) {

	}

}
