package collection;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {
		TreeSetDemo td = new TreeSetDemo();
		//td.treeset();
		//td.kapildev();
		//td.sachin();
		td.marks();
		//td.altermark();
		
	}
	
	private void altermark() {
		ArrayList ml = new ArrayList();
		Scanner sc = new Scanner(System.in);
		while(true) 
		{
			System.out.println("Enter mark");
			String mark = sc.next();
			try {
				Integer i = Integer.parseInt(mark);
				ml.add(i);
			}
			catch(Exception e){
				break;
			}
		
		}
		System.out.println(ml);
		
	}

	private void marks() {
		ArrayList ml = new ArrayList();
		Scanner sc = new Scanner(System.in);
		System.out.println("Do you want proceed? If not,say stop");
		String input = sc.next();
		
		while(true) {
			System.out.println("Enter mark:");
			int mark = sc.nextInt();
			ml.add(mark);
			System.out.println("Do you want proceed? If not,say stop");
			input = sc.next();
			if(input.equals("stop"))
				break;
		}
		System.out.println(ml);
		
	}

	private void sachin() {
		ArrayList sa = new ArrayList();
		
		sa.add(150);
		sa.add(65);
		sa.add(48);
		sa.add(77);
		sa.add(100);
		
		System.out.println(sa);
		int total = 0;
		for(Object score:sa) {
			Integer sc = (Integer)score;//Auto unboxing
			total = total + sc;
			if(sc <= 50)
			{
				System.out.println(score);
			}
		}
		System.out.println(total);
	}
	
	private void treeset() {
		
		TreeSet ts = new TreeSet();//ordering dictionary ,no duplicate lexicon order(compareTo method)
		ts.add("ram");
		ts.add("som");
		ts.add("ragu");
		ts.add("babu");
		ts.add("som");
		
		System.out.println(ts);

	}


	private void kapildev() {
		ArrayList al = new ArrayList();
		al.add("kapil");
		al.add(150);
		al.add(23);
		al.add(43.5);
		al.add(true);
		al.add(false);
		
		System.out.println(al);
		
		for(Object obj:al) {
			//System.out.print(obj+" ");
			try {
				//type casting
				//parent object to child object
				Integer sc = (Integer)obj;//runtime exception
				System.out.println(sc);
			}
			catch (Exception e) {
				
			}
		}
	}

}
