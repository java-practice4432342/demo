package collection;

import java.util.Arrays;
import java.util.Comparator;

public class ArrayDemo {

	public ArrayDemo() {
	}

	public static void main(String[] args) {
		
		int[] arr = {11,2,31,24,14,51};
		
		System.out.println("\nArry element before sort");
		
		for(int no:arr) {
			System.out.print(no+" ");
		}
		
		Arrays.sort(arr);
		
		System.out.println("\nArry element after sort");
		for(int val : arr) {
			System.out.print(val+" ");
		}
		
		System.out.println("\nArry element before sort");
		String names[] = {"ram","asam","raj","sarat"};
		for(String na:names) {
			System.out.print(na+" ");
		}
		Arrays.sort(names);
		
		System.out.println("\nArry element after sort");
		for(String na:names) {
			System.out.print(na+" ");
		}
		String names1[] = {"ram","asam","raj","sarath"};
		System.out.println(Arrays.equals(names, names1));
		Comparator comp = new  ComparatorDemo();
	}

}
