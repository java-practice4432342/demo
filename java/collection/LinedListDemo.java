package collection;

import java.util.LinkedList;

public class LinedListDemo {

	public static void main(String[] args) {
		
		LinkedList ll = new LinkedList();
			ll.add(5);
			ll.add(10);
			ll.add("sarath");
			ll.add("kumar");
			ll.add(10.5);
			ll.add(true);
		
			System.out.println(ll);
			ll.addFirst(100);
			System.out.println(ll);
			ll.addLast(200);
			System.out.println(ll);
			ll.poll();
			System.out.println(ll);
			ll.offer(1);
			ll.offer(11);
			ll.poll();
			System.out.println(ll);
			System.out.println(ll.get(4));
			System.out.println(ll.indexOf(1));
			System.out.println(ll);
			System.out.println(ll.remove(2));
			System.out.println(ll);
			System.out.println(ll.set(0, ll.get(4)));
			System.out.println(ll);
			System.out.println(ll.subList(1, 4));
			System.out.println(ll);
			System.out.println(ll.iterator());
			System.out.println(ll);
			System.out.println(ll.listIterator(1));
			System.out.println(ll.addAll(2,ll));
			System.out.println(ll);
			
			
	}

}
