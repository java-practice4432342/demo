package collection;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public class LinedHashSetDemo {

	public static void main(String[] args) {
		LinkedHashSet lhs = new LinkedHashSet();
		lhs.add("hi");
		lhs.add(5);
		lhs.add(true);
		lhs.add(10.5f);
		lhs.add("hi");
		lhs.add("bye");
		System.out.println(lhs.equals(lhs));
		System.out.println(lhs);
		
		ArrayList al = new ArrayList();
		al.add('s');
		al.add('a');
		al.add('r');
		al.add('a');
		al.add('t');
		al.add('h');
		System.out.println(al);
		LinkedHashSet lhsdemo = new LinkedHashSet(al);
		System.out.println(lhsdemo);
	}

}
