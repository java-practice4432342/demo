package collection;

import java.util.ArrayList;
import java.util.Collections;

public class CollectionsDemo {

	public static void main(String[] args) {
		ArrayList ls = new ArrayList();
		
		Laptop dell = new Laptop(45000,8,true,"g15");
		Laptop lenovo = new Laptop(34000,4,false,"v15");
		Laptop hp = new Laptop(50000,8,false,"h15");
	
		ls.add(dell);
		ls.add(lenovo);
		ls.add(hp);	
		System.out.println(hp.hashCode());
		System.out.println(dell.model.compareTo(lenovo.model));
		System.out.println(ls);	
		ComparatorDemo cd = new ComparatorDemo();
		Collections.sort(ls,cd);
		System.out.println(ls);
		Collections.reverse(ls);
		System.out.println(ls);
		
	}

}
