package collection;

import java.util.ArrayList;

public class ArrayListDemo {

	public static void main(String[] args) {
				ArrayList al = new ArrayList();
					al.add(5);
					al.add(10);
					al.add("sarath");
					al.add("kumar");
					al.add(10.5);
					al.add(true);
					
					System.out.println(al);
					System.out.println(al.get(4));
					System.out.println(al.indexOf(1));
					System.out.println(al);
					System.out.println(al.remove(2));
					System.out.println(al);
					System.out.println(al.set(0, al.get(4)));
					System.out.println(al);
					System.out.println(al.subList(1, 4));
					System.out.println(al);
					System.out.println(al.iterator());
					System.out.println(al);
					System.out.println(al.listIterator(1));
					System.out.println(al.addAll(2,al));
					System.out.println(al);
					System.out.println(al.size());
	}

}
