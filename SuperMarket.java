public class SuperMarket
{
	//this is field variables.
	static String shopName = "Sarath shop";
	static int doorNo = 5;
	
	//this is instance variable.
	String name;
	int price;
	
	// create constuctor 
	public SuperMarket()
	{
		System.out.println("I am a constutor with no args");
	}
	public SuperMarket(int a)
	{
		this.price = a;
	}
	public static void main(String[] args)
	{
		//this is product1 new object variables
		SuperMarket product1 = new SuperMarket();
		// we can access static variable using class name or object name.
			product1.name = "soap";//local variables
			product1.price = 30;
		System.out.println("Product1 Name :" +product1.name);
		System.out.println("Product1 Price :" +product1.price);
		//this is product2 new object and variables
		SuperMarket product2 = new SuperMarket();
		// we can access static variable using class name or object name.
			product2.name = "dal";//local variables
			product2.price = 300;
		System.out.println("Product2 Name :" +product2.name);
		System.out.println("Product2 Price :" +product2.price);
		
		
		//constructor line
		SuperMarket product3 = new SuperMarket(1000);
		product3.viewnum();
	}
	public void viewnum()
	{
		System.out.println("price "+" "+price);
	}
}
