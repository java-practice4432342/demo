const fm=document.querySelector("#form")
const uname=document.querySelector("#username")
const mail=document.querySelector("#email")
const psw=document.querySelector("#password")
const cpsw=document.querySelector("#cpassword")

fm.addEventListener('submit',(params)=>{
    if(ValidateInputes()==false)
    params.preventDefault();
})
function ValidateInputes() {
    const uname_val = uname.value
    const mail_val = mail.value
    const psw_val = psw.value
    const cpsw = cpsw.val
}
 let success=true;
//Enter name and validation
if (uname_val==="") {
    success=false;
    setError(uname,'User name req')
}
else{
    setSuccess(uname)
}
//Enter mail check validation
if(mail_val===""){
    success=false;
    setError(mail,'Email id req')
}
else if(validateEmail(mail_val)==false){
    success=false;
    setError(mail,'Invalid Email')
}
else{
    setSuccess(mail)
}
// Enter Password 
if (psw_val==="") {
    success=false;
    setError(psw,'Enter pass')
}
else if(psw_val.length<8){
    success=false;
    setError(psw,'Enter min 8 pass')
}
else{
    setSuccess(psw)
}
//conform password
if (cpsw_val==="") {
    success=false;
    setError(cpsw,'Enter pass')
}
else if(psw_val !== cpsw_val){
    success=false;
    setError(cpsw,'Enter min 8 pass')
}
else{
    setSuccess(cpsw)
}
