const fm=document.querySelector('#form')
const uname=document.querySelector('#username')
const mail=document.querySelector('#email')
const psw=document.querySelector('#password')
const cpsw=document.querySelector('#cpassword')

fm.addEventListener('submit',(params)=>{
    if(ValidateInputes()==false)
    params.preventDefault();
})
function ValidateInputes()
{
    const uname_val = uname.value
    const mail_val = mail.value
    const psw_val = psw.value
    const cpsw_val = cpsw.value

let success=true;
//Enter name and validation
if(uname_val==="") {
    success=false;
    setErr(uname,'User name req')
}
else{
    setSucc(uname)
}
//Enter mail check validation
if(mail_val===""){
    success=false;
    setErr(mail,'Email id req')
}
else if(validateEmail(mail_val)==null){
    success=false;
    setErr(mail,'Invalid Email')
}
else{
    setSucc(mail)
}
// Enter Password 
if (psw_val==="") {
    success=false;
    setErr(psw,'Enter pass')
}
else if(psw_val.length<8){
    success=false;
    setErr(psw,'Enter min 8 pass')
}
else{
    setSucc(psw)
}
//conform password
if (cpsw_val==="") {
    success=false;
    setErr(cpsw,'Enter pass')
}
else if(psw_val !== cpsw_val){
    success=false;
    setErr(cpsw,'Enter min 8 pass')
}
else{
    setSucc(cpsw)
}

return success
}

function setErr(ele,msg) {
    let input_group = ele.parentElement
    let error_element = input_group.querySelector('.error')

    error_element.innerHTML = msg
    input_group.classList.add('error')
    input_group.classList.remove('success')
}
function setSucc(ele) {
    let input_group = ele.parentElement
    let error_element = input_group.querySelector('.error')

    error_element.innerHTML = ""
    input_group.classList.add('success')
    input_group.classList.remove('error')
}
function validateEmail(ev) {
    return String(ev).toLowerCase().match(
        /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
      );
  };