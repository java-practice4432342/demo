const form = document.querySelector('#form');
const uname = document.querySelector('#name');
const mail = document.querySelector('#mail');
const pass = document.querySelector('#password');
const cpass = document.querySelector('#cpassword');

form.addEventListener('submit',(e)=>{
    e.preventDefault();
    validateInputs();
})
function validateInputs(){
    const uname_val = uname.value
    const mail_val = mail.value
    const pass_val = pass.value
    const cpass_val = cpass.value
    if(uname_val=="")
    {
        setErr(uname,"name is required");   
    }
    else{
        setSuccess(uname);
    }
    if(mail_val=="")
    {
        setErr(mail,"Email is required");   
    }
    else{
        setSuccess(mail);
    }
    if(pass_val=="")
    {
        setErr(pass,"pass is required");   
    }
    else{
        setSuccess(pass);
    }
    if (cpass_val=="")
    {
        setErr(cpass,"ReEnter password");   
    }
    else{
        setSuccess(cpass);
    }
}
function setErr(ele,msg){
    const in_group = ele.parentElement;
    const err_element = in_group.querySelector('.error');

    err_element.innerText = msg;
    in_group.classList.add('error')
    in_group.classList.remove('success')
}
function setSuccess(ele){
    const in_group = ele.parentElement;
    const err_element = in_group.querySelector('.error');

    err_element.innerText = "";
    in_group.classList.add('success')
    in_group.classList.remove('error')
}