let button=document.querySelector('button')
let result=document.createElement('div')
result.id='result'
document.getElementById('wrapper').appendChild(result)

button.addEventListener('click',()=>{
    let input=document.querySelector('#input')
   let city= input.options[input.selectedIndex].value
    let population
    let GrowthRate
    let language
    switch (city) {
        case 'Chennai':
            population = 11933000
            GrowthRate = 2.37
            language = 'Tamil'
            break;
        case 'Bengaluru':
            population = 13608000
            GrowthRate = 3.15
            language = 'Kannada'
            break;
        case 'Mumbai':
            population = 21,296,517
            GrowthRate = 1.6
            language = 'Marathi'
            break;
    
        default:
            
            break;
    }

   //console.log(city)
   let txt=`The current population of ${city} is ${population} as of today, based on worldometer elaboration of the latest united Nations data. Growing rate is ${GrowthRate} People who spoken language is ${language}.`
   result.innerHTML=txt
    
 })
 