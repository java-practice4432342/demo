package taskProSession;

public class D3_PStask {

	public static void main(String[] args) 
	{
		D3_PStask d3task = new D3_PStask();
		//d3task.looptask1();
		//d3task.looptask2();
		//d3task.looptask3();
		d3task.looptask4();
	}
	private void looptask4() 
	{
		//output = 2 4 6 8 10
		
		// idea 1
		System.out.println("Output:");
		int val = 1;
		while(val<=5) {
			System.out.print(val*2+" ");
			val = val + 1;
		}
		System.out.println();
		
		
		//idea 2
		int val2 = 1;
		while(val2<=10) {
			val2 = val2 + 1;
			System.out.print(val2+" ");
			val2 = val2 + 1;
		}
		System.out.println();
		
		//idea 3
		int val3 = 1;
		while(val3<=10) {
			if(val3%2==0) {
				System.out.print(val3+" ");
			}
			val3 = val3+1; 
		}
		
		
		
		
	}
	private void looptask3() 
	{
		System.out.println();
		int no = 1;
		while(no<=10) {
			System.out.print(no+" ");
			no = no + 1;
		}
		
	}

	private void looptask2() 
	{
		System.out.println();
		int no = 3;
		while(no<=15) {
			System.out.print(no+" ");
			no = no + 3;
		}
		
	}

	private void looptask1() 
	{
		//output 2 4 6 8 10
		//1)idea
		System.out.println("Output:-");
		int n = 2;
		while(n<=10) 
		{
			System.out.print(n+" ");
			n = n + 2;
		}
	}

}
