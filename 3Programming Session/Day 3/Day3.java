package programSession;
public class Day3
{
	public static void main(String[] args) 
	{
		Day3 d3 = new Day3();
		//d3.checkPrint();
		//d3.check1();
		//d3.check2();
		d3.looping1();
		d3.looping2();
	}

	private void looping2() 
	{
		//output 12345
		// 1)Known to unknown
				System.out.println("Output :");
				System.out.print(1+" ");
				System.out.print(2+" ");
				System.out.print(3+" ");
				System.out.print(4+" ");
				System.out.print(5+" ");
				
				
				//simplify repeated action simplified
				System.out.println();
				int no = 1;
				System.out.print(no+" ");
				no = no +1;
				System.out.print(no+" ");
				no = no +1;
				System.out.print(no+" ");
				no = no +1;
				System.out.print(no+" ");
				no = no +1;
				System.out.print(no+" ");
				no = no +1;
				System.out.println();
				//try to looping - while or for 
				
				System.out.println("While output: ");
				int row = 1;
				while(row<=5) 
				{
					System.out.print(row+" ");
					row = row + 1;
				}
		
	}

	private void looping1() 
	{
		/*Looping Rules
		 
		 * 1)Known to unknown
		 * 2)Don't think Entire output
		 * 3)Think about very Next output 
		 * 4)Introduce new variable
		 * 5)Micro to Macro
		 * 
		 * output ===== 1 1 1 1 1 => five times prints single line
		 */
		
		// 1)Known to unknown
		System.out.println("Output :");
		System.out.print(1 +" ");
		System.out.print(1+" ");
		System.out.print(1+" ");
		System.out.print(1+" ");
		System.out.print(1+" ");
		
		
		//simplify repeated action simplified
		System.out.println();
		int count = 0;
		System.out.print(1+" ");
		count = count +1;
		System.out.print(1+" ");
		count = count + 1;
		System.out.print(1+" ");
		count = count + 1;
		System.out.print(1+" ");
		count = count + 1;
		System.out.print(1+" ");
		count = count + 1;
		System.out.println();
		//try to looping - while or for 
		
		System.out.println("While output: ");
		int row = 1;
		while(row<=5) 
		{
			System.out.print(+1+" ");
			row = row + 1;
		}
		System.out.println();
	}

	private void check2() 
	{
		int no = 100;
		if(no<=100)
		{
			if(no%2==0) 
				System.out.println("even num: "+ no);
			else 
				System.out.println("odd num");
		}
	}
	private void check1()
	{	
		//boolean check
		boolean raining = false;
		if(raining)
			System.out.println("take umbralla");
		else
			System.out.println("not need to umbralla");
		//boolean check
		if(raining==true)
			System.out.println("take it umbralla");
		else
			System.out.println("no need to  take umbralla");
	}
	private void checkPrint() 
	{
		System.out.println(1);//print first 1 after send curse new line 
		System.out.print(2);//again print 2 no new line
		System.out.println(3);//2 3 print again go to new line
		System.out.print(4);//print 4	
		System.out.println();
	}

}
