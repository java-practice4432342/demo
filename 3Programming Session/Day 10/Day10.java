package programSession;

public class Day10 {

	public static void main(String[] args) {
		//
		Day10 d10 = new Day10();
		//d10.factorials();
		//d10.perfectNo();
		//d10.primeNo();
		d10.squreRoot();
		//d10.cubeRoot();
		//d10.fibonacci();
		
		
	}

	private void fibonacci() {
		int a = 0; int b =1;
		while(a < 15) {
			System.out.print(" "+a);
			int c =  a + b;
			a = b;
			b = c;
		}
		
	}

	private void cubeRoot() {
		int no = 8;
		int div = 2;
		while(div <= no/2) 
		{
			if(no/(div*div) ==  div)
			{
				System.out.println("cuberoot is "+div);
				break;
			}
			div++;
		}
		
	}

	private void squreRoot() {
		int no = 16;
		int div = 2;
		while(div<=no/2) {
			if(no/div ==div) {
				System.out.println("squre is "+div);
				break;
			}
			div++;
		}
		
	}

	private void primeNo() {
		boolean prime = true;
		int no =  12;
		int div = 2;
		while(div<no/2) {
			if(no%div == 0) {
				System.out.println("not a prime");
				prime = false;
				break;
			}
			div++;
		}
		if(prime == true) {
			System.out.println("prime number");
		}
	}

	private void perfectNo() {
		int n = 6;
		int sum = 0;
		int div = 1;
		while(div < n) 
		{
			if(n%div == 0) 
			{
				sum = sum + div;
			}
			div++;
		}
		System.out.println(sum);
		if(n == sum ) {
			System.out.println("this is perfect no");
		}
		else {
			System.out.println("not perfect");
		}
		
	}

	private void factorials() 
	{
		int no = 5; int sum = 0; int temp = no;
		while(no > 0) 
		{
			int digit = no % 10;
			int fact = find_fact(digit);
			//System.out.println(fact);
			sum = sum + fact;
			no = no/10;
		}
		System.out.println("this is "+ temp +"! factorial values = "+ sum);
	}
	//find factorial
	private int find_fact(int fno) {
		
		int result = 1;
		while(fno >0) {
			result = result * fno;
			fno--;
		}
		return result;
	}

}
