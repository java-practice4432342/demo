package programSession;
public class Day16Array 
{
	public static void main(String[] args)
	{
		Day16Array d16 = new Day16Array();
		d16.arrayjoin();
		d16.arrayjoin2();
		d16.arrayjoin2alt();
		d16.passvalidation();	
	}
	private void passvalidation()
	{
		char[] password = {'a','B','c','0','9','%'};
		//1.length = 6  2.number 3.special char 4.cap
		int no_count = 0;int spl_count = 0;
		int cap_count = 0;int sml_count=0;
		for(int i=0; i<password.length; i++) 
		{
			//System.out.print(password[i]+" ");
			if(password[i]>='A' && password[i]>='Z')
				cap_count++;
			else if(password[i]>='0' && password[i]>='9')
				no_count++;
			else if(password[i]>='a' && password[i]>='z')
				sml_count++;
			else
		        spl_count++;	
		}
		if(password.length >=6 && (no_count>=1 && cap_count>=1 && spl_count>=1))
			System.out.println("Password valid");
		else
			System.out.println("Not valid");
	}
	private void arrayjoin2alt() 
	{
		int[] x = {10,20,30,40,50,60};//inner loop
		int[] y = {10,30,50};//outer loop
		boolean sub = false;
		for(int i=0; i<y.length;i++) 
		{	sub = false;
			for(int j=0; j<x.length;j++) 
			{
				if(y[i] == x[j])
					sub = true;
					break;
			}
			if(sub == false)
				System.out.println("2This is not sub Array...");
				break;
		}
		if(sub == true ) {
			System.out.println("2This is sub Array");
		}
	}
	private void arrayjoin2() 
	{
		int[] x = {10,20,30,40,50,60};//inner loop
		int[] y = {10,30,50};//outer loop
		int count = 0;
		
		for(int i=0; i<y.length;i++) 
		{
			int z = y[i];
			
			for(int j=0; j<x.length;j++) 
			{
				if(y[i] == x[j])
				{
					count++;
					break;
				}
			}
		}
		if(count==y.length) {
			System.out.println("This is sub Array");
		}
		else 
		{
			System.out.println("This is not sub Array");
		}
	}
	private void arrayjoin() 
	{
		int x[] = {1,2,3,4,5,6};
		int y[] = {7,8,9,10};
		int z[] = new int[x.length+y.length];//10 
		//System.out.println(z.length);
		for(int i=0; i<z.length;i++) 
		{
			//System.out.print(z[i]+" ");
			if(i<y.length) 
			{
				z[i] = y[i];
				System.out.print(z[i]+" ");
			}
			else
			{
				z[i] = x[i-y.length];//i-4=0 x[0];
				System.out.print(z[i]+" ");
			}
		}
		System.out.println();
	}

	
}
