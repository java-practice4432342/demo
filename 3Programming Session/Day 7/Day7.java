package programSession;

public class Day7 {

	public static void main(String[] args) 
	{
		Day7 d7 = new Day7();
		d7.check1();
		d7.check2();
		d7.check3();
		d7.check4();
		d7.check5();
		//d7.check6();//mistake
		
	}
	private void check6() 
	{
		int n = 2;
		while(n<=5) {
			find_Power(2,5);
			n++;
		}
		
	}
	private void check5() 
	{
		int no =1;
		while(no<=5) 
		{
			find_Power(no,no);
			//System.out.println(no);
			no++;
		}
	}
	private void find_Power(int base , int power) 
	{
		int result = 1;
		while(power>1) 
		{
			result = result * base;
			power--;
		}
		System.out.print(result+" ");
	}

	private void check4() 
	{
		int no = 1;
		while(no<=5) 
		{
			int base = no , power = no;
			int res = 1;
			while(power>0) 
			{
				res = res * base;
				power--;
			}
			System.out.println(res);
			no++;
		}
		
	}

	private void check3() 
	{
		int no1= 10, no2 = 8;
		while(no2==0) 
		{
			System.out.println(no1);
			no1 = no1 + no2;
			no2 = no2- 2;
		}
	}

	private void check2() 
	{
		int no1= 1,no2 = 10;
		while(no1<=5)
		{
			System.out.println(no1+"*"+no2+"="+(no1*no2));
			no1++;no2--;
		}
		
	}

	private void check1() 
	{
		int no1= 1,no2 = 10;
		while(no1<=5) {
			System.out.println(no1*no2);
			no1++;no2--;
		}
	}

}
