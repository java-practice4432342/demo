package ProgramingSession;

public class Recursion {

	public static void main(String[] args) 
	{
		Recursion rec = new Recursion();
		System.out.println("factorial of 4 is => "+rec.find_fact(4));
	}

	private int find_fact(int no) 
	{
		if(no==1) return no;
		return no * find_fact(no-1);
	}

}
