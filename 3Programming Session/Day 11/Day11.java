package programSession;

public class Day11 {

	public static void main(String[] args) 
	{
		Day11 d11 = new Day11();
		d11.forloop();
		d11.pattern1();
		d11.pattern2();
		d11.pattern3();
		d11.pattern4();
		d11.pattern5();
		d11.pattern6();
		d11.pattern7();
		//d11.pattern8();
	}
	
	private void pattern8()
	{
		
	}

	private void pattern7() 
	{
		System.out.println();
		int key=1;
		for(int row = 5; row >= 1; row--)
		{
			//System.out.print(key);
			for(int no =1 ; no <= row; no++) {
				System.out.print(key+" ");
				key++;
			}
			System.out.println();
		}
	}

	private void pattern6() 
	{
		System.out.println();
		for(int row = 1; row<=9; row = row + 2) 
		{
			for(int no = row; no<=9; no+=2)
			{
				//System.out.print(no+" ");
				//System.out.print(row +" ");
			}
			System.out.println();
		}
	}

	private void pattern5() 
	{
		//System.out.println();
		for(int row = 1; row <=5; row++)
		{
			for(int no = row; no<=5;no++) 
			{
				System.out.print(no+" ");
			}
			System.out.println();
		}
		
	}

	private void pattern4() 
	{
		for(int row = 5; row >= 1; row--)
		{
			for(int no = 1; no <= row; no++)
			{
				System.out.print(no +" ");
//				System.out.print(row +" ");
//				System.out.print("* ");
			}
			System.out.println();
		}
		
	}

	private void pattern3() 
	{
		for(int no = 5; no>=1; no--) 
		{
			for(int row = 1; row <= no;row++) 
			{
				//System.out.print("* ");
				System.out.print(row+" ");
				//System.out.print(no+" ");
			}
			System.out.println();
		}
		
	}

	private void pattern2() 
	{
		int no = 5;
		while(no>=5)
		{
			for(int row = 1;row <= no; row++) 
			{
				//System.out.print("* ");
				//System.out.print(row +" ");
				System.out.print(no+" ");
			}
			System.out.println();
			no--;
		}
		
	}

	private void pattern1()
	{
		for(int count = 1; count <= 3;count++) {
			for(int row=1; row<= 5;row++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
	}
	private void forloop() 
	{
		for(int no = 1;no <= 5;no++) 
		{
			//System.out.print("*"+" ");
			System.out.print(no+" ");
		}
		System.out.println();
		
	}
}
