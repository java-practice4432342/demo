package programSession;

public class Day15Array {

	public static void main(String[] args) 
	{
		Day15Array d15 = new Day15Array();
		d15.array1();
		d15.array2();
		d15.array3();
		d15.array4();
	}
	
	
	private void array4() 
	{
		int[] bill = {230,150,375,40,350,250};
		int first = 0,second = 0;
		for(int i = 0;i<bill.length;i++)
		{
			if(bill[i]>first)
			{
				second = first;
				first = bill[i];
			}
			else if(bill[i]>second) 
			{
				second = bill[i];
			}
		}
		System.out.println("first big number "+first);
		System.out.println("second big number "+second);
		
	}
	private void array3() 
	{
		// count of array
		int[] bill = {230,150,375,400,150,150};
		int count = 0;int repeat = 150;
		for(int i = 0;i<bill.length;i++)
		{
			if(bill[i] == repeat)
				count++;
		}
		System.out.println("The  repeat("+repeat+") number  count is "+count);
		
	}
	private void array2()
	{
		//searching program
		int[] bill = {230,450,375,400};
		String[] months = {"january","February","March","April"};
		int search = 450;
		for(int i=0; i<bill.length;i++)
		{
			if(bill[i]==search)
				System.out.println("yes found at "+months[i]+" = "+search);
		}
		
	}
	private void array1() 
	{
		int[] bill = {230,450,375,400};
		int big = bill[0], small = bill[0],i=1;
		while(i<bill.length) 
		{
			if(bill[i]>i)
				big = bill[i];
			else if(bill[i]<i)
				small = bill[i];
			i++;
		}
		System.out.println("the big number is "+big);
		System.out.println("the small number is "+small);	
	}
}
