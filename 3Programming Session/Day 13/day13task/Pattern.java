public class Pattern
{
	public static void main(String[] arg)
	{
		Pattern pt = new Pattern();
		pt.patt1();
		pt.patt2();
		pt.patt3();
		pt.patt4();
		pt.patt5();
		pt.patt6();
		pt.patt7();
		pt.patt8();
		pt.patt9();
		pt.patt10();	
	}
	public void patt1()
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1; col<=row; col++)
				System.out.print(row+" ");
	
			System.out.println();
		}
		System.out.println();
	}
	public void patt2()
	{
		for(int row=5; row>=1; row--)
		{
			for(int col=5; col>=6-row; col--)
				System.out.print(row+" ");
				
			System.out.println();
		}
		System.out.println();
	}
	public void patt3()
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1;col<row;col++)
			{
				System.out.print(col+" ");
			}
			System.out.println();
		}
		System.out.println();
	}
	public void patt4()
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1;col<row;col++)
			{
				System.out.print(col+" ");
			}
			System.out.print("* ");
			System.out.println();
		}
		System.out.println();
	}
	public void patt5()
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1;col<row;col++)
			{
				System.out.print(col+"  ");
			}
			for(int star=1; star<=5-row; star++)
				System.out.print("*  ");
			System.out.println();
		}
		System.out.println();
	}
	public void patt6()
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1;col<=row;col++)
				System.out.print("* ");
			for(int star=1; star<=5-row; star++)
				System.out.print(star+" ");
			System.out.println();
		}
		System.out.println();
	}
	public void patt7()
	{
		for(char row='A'; row<='E'; row++)
		{
			for(char col='A';col<='E';col++)
				System.out.print(col+" ");
			System.out.println();
		}
		System.out.println();
	}
	public void patt8()
	{
		for(char row='A'; row<='E'; row++)
		{
			for(char col='A';col<=row;col++)
				System.out.print(col+" ");
			System.out.println();
		}
		System.out.println();
	}
	public void patt9()
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1; col<=5; col++)
			{
				if(col%2==0)
					System.out.print(0+" ");
				else
					System.out.print(1+" ");
			}
			System.out.println();
		}
		System.out.println();
	}
	public void patt10()
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1; col<=5; col++)
			{
				if(row%2==0)
					System.out.print(0+" ");
				else
					System.out.print(1+" ");
				
			}
			System.out.println();
		}
		System.out.println();
	}
	

} 
