package programSession;

public class Day13 {

	public static void main(String[] args) {
		
		Day13 d13 = new Day13();
		d13.pattern1();
		d13.pattern2();
		d13.pattern3();
		d13.pattern4();
		d13.pattern5();
		d13.pattern6();
		d13.pattern7();
		d13.pattern8();
		d13.pattern9();
		d13.pattern10();
		
	}
	
	private static void pattern10()
	{
		for(int  row=1;row<=5; row++) 
		{
			for(int col=1; col<=row; col++)
				System.out.print(col+" ");
			System.out.println();
		}
		
		System.out.println();
	}

	private void pattern9() 
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1; col<6-row;col++)
				System.out.print(" "+" ");
			for(int num=1;num<=row;num++)
				System.out.print(num+"   ");
			System.out.println();
		}
		System.out.println();
		
	}

	private void pattern8() 
	{
		for(int row=1; row <= 5; row++)
		{
			for(int col=1;col<=6-row;col++)
				System.out.print(" "+" ");
			for(int star=1; star<=row; star++)
				System.out.print("*   ");
			System.out.println();
		}
		System.out.println();
	}

	private void pattern7() 
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1; col<6-row; col++)
				System.out.print(col+" ");
			for(int star=1; star<=row;star++)
				System.out.print("* ");
			System.out.println();
		}
		System.out.println();
	}
	private void pattern6() 
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1; col<6-row;col++) 
				System.out.print(col+" ");
			for(int star=1; star<=6-row;star++)
				System.out.print("* ");
			System.out.println();
		}
		System.out.println();
	}
	private void pattern5()
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1; col<6-row;col++)
				System.out.print(col+" ");
			System.out.print("* * *");
			System.out.println();
		}
		System.out.println();
	}
	private void pattern4() 
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1; col<6-row;col++)
				System.out.print(col+" ");
			System.out.print("*");
			System.out.println();
		}	
		System.out.println();
	}
	private void pattern3() 
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1; col<6-row;col++)
				System.out.print(col+" ");
			System.out.println();
		}
		System.out.println();
	}
	private void pattern2()
	{
		for(int row=1; row<5; row++)
		{
			for(int col=1; col<=6-row;col++)
				System.out.print(col+" ");
			System.out.println();
		}
		System.out.println();
	}
	private void pattern1()
	{
		for(int row=1; row<=5; row++) 
		{
			for(int col=1;col<=6-row;col++) 
				System.out.print(col+" ");
			System.out.println();
		}
		System.out.println();
	}

}
