public class Datatypes
{
	public static void main(String[] args)
	{
		//this is words or strings
		System.out.println("hi sarath");
		
		//this is store number =>byte 
		byte tamil = 60;
		System.out.println("tamilMarks =" + tamil);
		
		//this is store number =>sort
		short money = 2000;
		System.out.println("money =" + money);
		
		//this is store number =>int
		int lack = 100000;
		System.out.println("lack ="+ lack);

        //this is store number =>-int
		int lacks = -10000000;
		System.out.println("lacks ="+ lacks);
		
		//this is store point =>float
		float pi = 3.14f;
		System.out.println("pi =" + pi);
		
		//this is store pointer =>double
		double infinity = 12.3456789;
		System.out.println("infinity= " + infinity);
		
		//this is store pointer =>boolean
		boolean rain=false;
		System.out.println("today is raining =" + rain);
		
	}

}
